#include "stdafx.h"
#include <iostream>
#include <omp.h>
//sections section
void funcA()
{
	printf("In funcA: this section is executed by thread %d\n", omp_get_thread_num());
}

void funcB()
{
	printf("In funcB: this section is executed by thread %d\n", omp_get_thread_num());
}

void main()
{
	#pragma omp parallel
	{
		#pragma omp sections
		{
			#pragma omp section
			{
				(void)funcA();
			}

			#pragma omp section
			{
				(void)funcB();
			}
		}
	}
}

