﻿#include "stdafx.h"
#include <iostream>
#include <omp.h>
#include <time.h>
#include <string>
/*
2. Napisati sekvencijalni program kojim se generiše skalarni proizvod dva vektora. Napisati
OpenMP program kojim se generiše skalarni proizvod dva vektora, podelom iteracija petlje
između različitih niti sa i bez korišćenja odredbe redukcije za kombinovanje parcijalnih rezultata
u nitima. Uporediti vremena izvršenja u oba slučaja sa sekvencijalnim vremenom izvršenja.
Uporediti ova rešenja za različite dimenzije vektora.
*/
using std::string;
using std::to_string;
using std::cout;
using std::endl;

int scalarProductSequential(int N, long* a, long* b, double* time);
int scalarProductWithReduction(int N, long* a, long* b, double* time);
int scalarProductWithoutReduction(int N, long* a, long* b, double* time);
string getRanking(double* arr);
void swap(double *xp, double *yp, string *s1, string *s2);
void fillArrays(long* a, long* b, int N);
void printArray(long* a, int N);

void main()
{
	double timeParallelWithReduction, timeParallelWithoutReduction, timeSequential;
	int arrayOfLengths[5] = { 100, 1000, 10000, 100000, 1000000 };
	double** matOfTimes = new double*[5];
	for (int i = 0; i < 5; i++)
		matOfTimes[i] = new double[3];

	for (int i = 0; i < 5; i++)
	{
		long* a = new long[arrayOfLengths[i]];
		long* b = new long[arrayOfLengths[i]];
		fillArrays(a, b, arrayOfLengths[i]);
		//printArray(a, arrayOfLengths[i]);
		//printArray(b, arrayOfLengths[i]);

		printf("Array length = %d =====================================================================================\n", arrayOfLengths[i]);
		scalarProductWithReduction(arrayOfLengths[i], a, b, &timeParallelWithReduction);
		scalarProductWithoutReduction(arrayOfLengths[i], a, b, &timeParallelWithoutReduction);
		scalarProductSequential(arrayOfLengths[i], a, b, &timeSequential);

		matOfTimes[i][0] = timeParallelWithReduction;
		matOfTimes[i][1] = timeParallelWithoutReduction;
		matOfTimes[i][2] = timeSequential;
	}

	printf("\n\nAfter all...\n");
	printf("Num elements | Time reduction | No reduction | Time sequential | Message\n");
	for (int i = 0; i < 5; i++)
	{
		printf("%12d | %12.11lf | %13.12lf | %13.12lf | ", arrayOfLengths[i], matOfTimes[i][0], matOfTimes[i][1], matOfTimes[i][2]);
		cout << getRanking(matOfTimes[i]) << endl;
	}
}

void printArray(long* a, int N)
{
	for (int i = 0; i < N; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}

void fillArrays(long* a, long* b, int N)
{
	for (int i = 0; i < N; i++)
	{
		a[i] = i;
		b[i] = N - i;
	}
}

void swap(double *xp, double *yp, string *s1, string *s2)
{
	double temp = *xp;
	*xp = *yp;
	*yp = temp;

	string tempStr = *s1;
	*s1 = *s2;
	*s2 = tempStr;
}

string getRanking(double* arr)
{
	string result = "";
	string aprouch[3] = { "With Reduction", "Without Reduction", "Sequential" };

	for (int i = 0; i < 3; i++)
	{
		result.append(to_string(i + 1) + ": ");
		int min = i;
		for (int j = i; j < 3; j++)
		{
			if (arr[j] < arr[min])
				min = j;
		}
		result.append(aprouch[min] + " ");

		swap(&arr[min], &arr[i], &aprouch[min], &aprouch[i]);
	}

	return result;
}

int scalarProductWithReduction(int N, long* a, long* b, double* time)
{
	double startTime, endTime, cpu_time;
	long product = 0;

	startTime = omp_get_wtime();
#pragma omp parallel shared(a,b) 
	{
#pragma omp for reduction(+:product) 
		for (int i = 0; i < N; i++)
			product = product + (a[i])*(b[i]);
	}

	endTime = omp_get_wtime();
	cpu_time = endTime - startTime;
	*time = cpu_time;

	printf("Reduction >>> Parallel >>> Time: %lf >>> Scalar product for N = %d is %ld\n", *time, N, product);

	return product;
}

int scalarProductWithoutReduction(int N, long* a, long* b, double* time)
{
	double startTime, endTime, cpu_time;
	long scalarGeneral = 0;
	long scalarLocal = 0;

	startTime = omp_get_wtime();

#pragma omp parallel shared(scalarGeneral) firstprivate(scalarLocal)
	{
#pragma omp for
		for (int i = 0; i < N; i++)
		{
			scalarLocal = scalarLocal + (a[i])*(b[i]);
		}

#pragma omp critical(update)
		{
			scalarGeneral = scalarGeneral + scalarLocal;
		}
	}

	endTime = omp_get_wtime();
	cpu_time = endTime - startTime;
	*time = cpu_time;

	printf("Without Reduction >>> Parallel >>> Time: %lf >>> Scalar product for N = %d is %ld\n", *time, N, scalarGeneral);

	return scalarGeneral;
}

int scalarProductSequential(int N, long* a, long* b, double* time)
{
	double startTime, endTime, cpu_time;

	startTime = omp_get_wtime();

	long product = 0;
	for (int i = 0; i < N; i++)
		product = product + (a[i])*(b[i]);

	endTime = omp_get_wtime();
	cpu_time = endTime - startTime;
	*time = cpu_time;

	printf("Sequential >>> Time: %lf >>> Scalar product for N = %d is %ld\n", *time, N, product);

	return product;
}

/*
Output:
Array length = 100 =====================================================================================
Reduction >>> Parallel >>> Time: 0.004111 >>> Scalar product for N = 100 is 166650
Without Reduction >>> Parallel >>> Time: 0.000032 >>> Scalar product for N = 100 is 166650
Sequential >>> Time: 0.000000 >>> Scalar product for N = 100 is 166650
Array length = 1000 =====================================================================================
Reduction >>> Parallel >>> Time: 0.000021 >>> Scalar product for N = 1000 is 166666500
Without Reduction >>> Parallel >>> Time: 0.000018 >>> Scalar product for N = 1000 is 166666500
Sequential >>> Time: 0.000003 >>> Scalar product for N = 1000 is 166666500
Array length = 10000 =====================================================================================
Reduction >>> Parallel >>> Time: 0.000039 >>> Scalar product for N = 10000 is -837059544
Without Reduction >>> Parallel >>> Time: 0.000057 >>> Scalar product for N = 10000 is -837059544
Sequential >>> Time: 0.000057 >>> Scalar product for N = 10000 is -837059544
Array length = 100000 =====================================================================================
Reduction >>> Parallel >>> Time: 0.000207 >>> Scalar product for N = 100000 is 460728720
Without Reduction >>> Parallel >>> Time: 0.000211 >>> Scalar product for N = 100000 is 460728720
Sequential >>> Time: 0.000465 >>> Scalar product for N = 100000 is 460728720
Array length = 1000000 =====================================================================================
Reduction >>> Parallel >>> Time: 0.002518 >>> Scalar product for N = 1000000 is 1183719328
Without Reduction >>> Parallel >>> Time: 0.002248 >>> Scalar product for N = 1000000 is 1183719328
Sequential >>> Time: 0.007984 >>> Scalar product for N = 1000000 is 1183719328


After all...
Num elements | Time reduction | No reduction   | Time sequential | Message
100			 | 0.00411095866  | 0.000032060081 | 0.000000427477  | 1: Sequential 2: Without Reduction 3: With Reduction
1000		 | 0.00002137339  | 0.000017526210 | 0.000003419700  | 1: Sequential 2: Without Reduction 3: With Reduction
10000		 | 0.00003932707  | 0.000057280762 | 0.000056853169  | 1: With Reduction 2: Sequential 3: Without Reduction
100000		 | 0.00020732195  | 0.000211169245 | 0.000464657671  | 1: With Reduction 2: Without Reduction 3: Sequential
1000000		 | 0.00251821335  | 0.002248481032 | 0.007983817719  | 1: Without Reduction 2: With Reduction 3: Sequential
*/

