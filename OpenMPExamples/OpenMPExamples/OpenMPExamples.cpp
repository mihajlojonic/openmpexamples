#include "stdafx.h"
#include <iostream>
#include <omp.h>
#include <time.h>
#include <string>
/*
6. Napisati OpenMP kod koji sadrži sledeću petlju:
for (i=0;i<m;i++)
for (j=0;j<n;j++)
a[i][j]=2*a[i-1][j];
i izvršiti njenu paralelizaciju. Proučiti da li postoje zavisnosti između iteracija i po kom indeksu
je moguća paralelizacija. Ispitati da li zamena petlji uzrokuje promenu u performansama.
*/
using std::string;
using std::to_string;
using std::cout;
using std::endl;

void fillMatrix(int** a, int N);
void printMatrix(int** a, int N);

void main()
{
	int N = 10;
	int** a = new int*[N];

	for (int i = 0; i < N; i++)
		a[i] = new int[N];

	printf("Array length = %d =====================================================================================\n", N);

	fillMatrix(a, N);
	printf("Initial matrix:\n");
	printMatrix(a, N);

	printf("\nSequential:");
	for (int i = 1; i < N; i++)
		for (int j = 0; j < N; j++)
			a[i][j] = 2 * a[i - 1][j];
	printMatrix(a, N);

	fillMatrix(a, N);
	printf("\nParallel >>> with swap:");
#pragma omp parallel shared(a)
	{
#pragma omp for
		for (int j = 0; j < N; j++)
			for (int i = 1; i < N; i++)
				a[i][j] = 2 * a[i - 1][j];
	}
	printMatrix(a, N);

	fillMatrix(a, N);
	printf("\nParallel >>> no swap:");
#pragma omp parallel shared(a)
	{
#pragma omp for
		for (int i = 1; i < N; i++)
			for (int j = 0; j < N; j++)
				a[i][j] = 2 * a[i - 1][j];
	}
	printMatrix(a, N);
}

void printMatrix(int** a, int N)
{
	for (int i = 0; i < N; i++)
	{
		printf("\n");
		for (int j = 0; j < N; j++)
			printf("%d ", a[i][j]);
	}
	printf("\n");
}

void fillMatrix(int** a, int N)
{
	for (int i = 0; i < N; i++)
		for (int j = 0; j < N; j++)
			a[i][j] = 1;
}

/*
Output:
Array length = 10 =====================================================================================
Initial matrix:

1 1 1 1 1 1 1 1 1 1
1 1 1 1 1 1 1 1 1 1
1 1 1 1 1 1 1 1 1 1
1 1 1 1 1 1 1 1 1 1
1 1 1 1 1 1 1 1 1 1
1 1 1 1 1 1 1 1 1 1
1 1 1 1 1 1 1 1 1 1
1 1 1 1 1 1 1 1 1 1
1 1 1 1 1 1 1 1 1 1
1 1 1 1 1 1 1 1 1 1

Sequential:
1 1 1 1 1 1 1 1 1 1
2 2 2 2 2 2 2 2 2 2
4 4 4 4 4 4 4 4 4 4
8 8 8 8 8 8 8 8 8 8
16 16 16 16 16 16 16 16 16 16
32 32 32 32 32 32 32 32 32 32
64 64 64 64 64 64 64 64 64 64
128 128 128 128 128 128 128 128 128 128
256 256 256 256 256 256 256 256 256 256
512 512 512 512 512 512 512 512 512 512

Parallel >>> with swap:
1 1 1 1 1 1 1 1 1 1
2 2 2 2 2 2 2 2 2 2
4 4 4 4 4 4 4 4 4 4
8 8 8 8 8 8 8 8 8 8
16 16 16 16 16 16 16 16 16 16
32 32 32 32 32 32 32 32 32 32
64 64 64 64 64 64 64 64 64 64
128 128 128 128 128 128 128 128 128 128
256 256 256 256 256 256 256 256 256 256
512 512 512 512 512 512 512 512 512 512

Parallel >>> no swap:
1 1 1 1 1 1 1 1 1 1
2 2 2 2 2 2 2 2 2 2
4 4 4 4 4 4 4 4 4 4
8 8 8 8 8 8 8 8 8 8
16 16 16 16 16 16 16 16 16 16
32 32 32 32 32 32 32 32 32 32
64 64 64 64 64 64 64 64 64 64
128 128 128 128 128 128 128 128 128 128
2 2 2 2 2 2 2 2 2 2
4 4 4 4 4 4 4 4 4 4
*/